Pod::Spec.new do |s|
    s.name         = 'Usercentrics'
    s.version      = '2.18.0'

    s.authors      = "Usercentrics"
    s.summary      = "Usercentrics SDK"
    s.description  = "Usercentrics Framework for iOS and tvOS"
    s.homepage     = "https://docs.usercentrics.com/cmp_in_app_sdk/latest/"

    s.license = {
      :type => "Copyright",
      :text => "Copyright 2023. Permission is granted to Usercentrics"
    }

    s.source       = { :http => "https://bitbucket.org/usercentricscode/usercentrics-spm-sdk/downloads/Usercentrics-2.18.0.xcframework.zip" }

    s.vendored_frameworks = "Usercentrics.xcframework"
    s.tvos.vendored_frameworks = "Usercentrics.xcframework"

    s.resource_bundles = { 'UsercentricsPrivacyInfo' => ['PrivacyInfo.xcprivacy'] }

    s.swift_version = '5.3'

    s.ios.deployment_target  = '11.0'
    s.tvos.deployment_target = '11.0'
end
