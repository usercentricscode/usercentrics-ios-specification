Pod::Spec.new do |s|
    s.name         = 'Usercentrics'
    s.version      = '2.11.4-unity-static'

    s.authors      = "Usercentrics"
    s.summary      = "Usercentrics SDK"
    s.description  = "Usercentrics Framework for Unity iOS"
    s.homepage     = "https://docs.usercentrics.com/cmp_in_app_sdk/latest/"

    s.license = {
      :type => "Copyright",
      :text => "Copyright 2023. Permission is granted to Usercentrics"
    }

    s.source       = { :http => "https://bitbucket.org/usercentricscode/usercentrics-spm-sdk/downloads/Usercentrics-2.11.4-unity-static.xcframework.zip" }

    s.vendored_frameworks = "Usercentrics.xcframework"

    s.swift_version = '5.3'

    s.ios.deployment_target  = '11.0'
end
