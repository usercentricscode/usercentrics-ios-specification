Pod::Spec.new do |s|
    s.name         = 'Usercentrics'
    s.version      = '2.0.2'

    s.authors      = "Usercentrics"
    s.summary      = "Usercentrics SDK"
    s.description  = "Usercentrics Framework for iOS and tvOS"
    s.homepage     = "https://docs.usercentrics.com/#/in-app-sdk"

    s.license = { :type => 'Copyright', :text => "
                  Copyright 2021
                  Permission is granted to Usercentrics
                "
               }

    s.source       = { :http => "https://bitbucket.org/usercentricscode/usercentrics-spm-sdk/downloads/Usercentrics-2.0.2.xcframework.zip" }

    s.vendored_frameworks = "Usercentrics.xcframework"
    s.swift_version = '5.3'

    s.ios.deployment_target  = '11.0'
    s.tvos.deployment_target = '11.0'
end
