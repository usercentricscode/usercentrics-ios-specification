Pod::Spec.new do |s|
    s.name         = 'UsercentricsUI'
    s.version      = '1.12.7'

    s.authors      = "Usercentrics"
    s.summary      = "Usercentrics UI SDK"
    s.description  = "Usercentrics UI Framework for iOS"
    s.homepage     = "https://docs.usercentrics.com/#/in-app-sdk"

    s.license = { :type => 'Copyright', :text => "
                  Copyright 2021
                  Permission is granted to Usercentrics
                "
               }

    s.source       = { :http => "https://bitbucket.org/usercentricscode/usercentrics-spm-ui/downloads/UsercentricsUI-1.12.7.xcframework.zip" }

    s.vendored_frameworks = "UsercentricsUI.xcframework"

    s.dependency 'Usercentrics', '1.12.7'

    s.platform = :ios
    s.swift_version = '5.3'
    s.ios.deployment_target = '11.0'
end
