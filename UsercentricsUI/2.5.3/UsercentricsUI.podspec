Pod::Spec.new do |s|
    s.name         = 'UsercentricsUI'
    s.version      = '2.5.3'

    s.authors      = "Usercentrics"
    s.summary      = "Usercentrics UI SDK"
    s.description  = "Usercentrics UI Framework for iOS"
    s.homepage     = "https://docs.usercentrics.com/cmp_in_app_sdk/latest/"

    s.license = { :type => 'Copyright', :text => "
                  Copyright 2021
                  Permission is granted to Usercentrics
                "
               }

    s.source       = { :http => "https://bitbucket.org/usercentricscode/usercentrics-spm-ui/downloads/UsercentricsUI-2.5.3.xcframework.zip" }

    s.vendored_frameworks = "UsercentricsUI.xcframework"

    s.dependency 'Usercentrics', '2.5.3'
    s.swift_version = '5.3'

    s.ios.deployment_target  = '11.0'
    s.tvos.deployment_target = '11.0'
  end
